#!/bin/bash

## colors {{
color="$(tput setaf 4)"
color2="$(tput setaf 6)"
reset="$(tput setaf 7)"
## }}

emptyTrash()
{
    echo -e "${color}::${reset} Empty trash can..."

    ktrash5 --empty
}

cleanDotFolders()
{
    echo -e "${color}::${reset} Deleting useless dot folders..."

    folders=( ".pki" ".rustup" ".cargo" )

    for folder in ${folders[@]}; do
        sudo rm -rf $folder
    done
}

cleanUselessFolders()
{
    echo -e "${color}::${reset} Cleaning /logs (created by OpenRGB)..."
    sudo rm -rf /logs
}

runSystemUpgrade()
{
    echo -e "${color}::${reset} Running system upgrade..."
    echo -e "${color2} ==>${reset} $(sudo ls /var/cache/pacman/pkg/ | wc -l) packages in cache"
    echo -e "${color2} ==>${reset} $(du -sh /var/cache/pacman/pkg/) in storage space"

    sudo pacman -Sy --needed --noconfirm || ( echo "Could not update system!"; exit )
    sudo pacman --needed --noconfirm -S archlinux-keyring || ( echo "Could not update system!"; exit )
    sudo pacman -Syu --needed --noconfirm || ( echo "Could not update system!"; exit )
}

cleanSystemCache()
{
    excluded_list=(dolphin-emu rpcs3 extra mesa_shader_cache mupen64plus)

    cd "${HOME}/.cache"

    for folder in *; do
        if [[ ! "${excluded_list[*]}" =~ "${folder}" ]]; then
            echo -e "${color2} ==>${reset} Cleaning ${folder} from system cache..."
            rm -rf "${folder}"
        fi
    done
}

cleanPackageCache()
{
    echo -e "${color}::${reset} Cleaning pacman cache..."
    paccache -rk 2 || ( echo "Could not clean up package cache!"; exit )
}

cleanLint()
{
    rmlint || ( echo "Could not clean up lint!"; exit )

    read -r -p "${color}::${reset} Clean lint? [y/N] " response

    case "$response" in
        [yY][eE][sS|[yY] ) bash "${HOME}/.cache/rmlint.sh" -d || \
                           ( echo "Could not clean up lint!"; exit );;
                       * ) ;;
    esac

    # Clean up .part files from failed upgrades
    find /var/cache/pacman/pkg/ -iname "*.part" -delete || ( echo "Could not clean up lint!"; exit )
}

cleanJournal()
{
    echo -e "${color}::${reset} Cleaning journal..."

    sudo journalctl --vacuum-time=4weeks || ( echo "Could not clean up journal!"; exit )
}

runBleachbit()
{
    trash=(
        deepscan.ds_store
        deepscan.thumbs_db
        deepscan.tmp
        discord.cache
        discord.vacuum
        firefox.crash_reports
        firefox.vacuum
        google_chrome.cache
        google_chrome.cookies
        google_chrome.dom
        google_chrome.form_history
        google_chrome.history
        google_chrome.search_engines
        google_chrome.session
        google_chrome.sync
        google_chrome.vacuum
        gwenview.recent_documents
        journald.clean
        rhythmbox.cache
        rhythmbox.history
        system.custom
        system.rotated_logs
        system.tmp
        system.trash
        thunderbird.cache
        thunderbird.vacuum
        x11.debug_logs
    )

    for item in "${trash[@]}"; do
        echo -e "${color2} ==>${reset} Running bleachbit job: ${item}"

        bleachbit --clean $item >/dev/null 2>&1 || ( echo "Could not clean $item"; exit 1 )
    done
}

fixPackageRepository()
{
    echo -e "${color}::${reset} Fixing package repository..."

    sudo LANG=C pacman -Qkk 2>&1 1>/dev/null | \
    awk '/^warning.* mismatch/ {print $3}'   | \
    sudo pacrepairfile --uid --gid --mode    || \
    ( echo "Could not fix package repository!"; exit )
}

cleanupOrphanedPackages()
{
    echo -e "${color}::${reset} Cleaning up orphaned packages..."

    sudo pacman -Qtdq | sudo pacman -Rns - || ( echo "Could not clean up orphaned packages!"; exit )
}

defragPartitions()
{
    MUSIK="5b4b2a9c-23b6-42a7-b5bb-6e835735a579"
    VIDYA="1d62efda-1735-4904-8d92-63aa58a73059"
    TV="7db6382e-2117-4d7d-a67b-cedacb87b347"
    MISC="53cfbdea-74cd-48a9-a6da-c6ac9ed000e8"

    partitions=( $MISC $MUSIK $TV $VIDYA )

    for partition in ${partitions[@]}; do
        isMounted=$(findmnt -noTARGET "/dev/disk/by-uuid/${partition}")
        deviceLabel="$(findfs UUID=${partition})"

        if [[ $isMounted ]]; then
            sudo umount $deviceLabel || ( echo "Could not unmount ${deviceLabel}!"; exit )
        fi

        echo -e "${color2} ==>${reset} Defragmenting ${deviceLabel}..."
        sudo e2fsck -f -v -C 0 -n $deviceLabel || ( echo "Could not defrag device ${deviceLabel}!"; exit )
    done
}

promptForDefrag()
{
    read -r -p "${color}::${reset} Defrag hard drive partitions? [y/N] " response

    case "$response" in
        [yY][eE][sS|[yY] ) defragPartitions;;
                       * ) ;;
    esac
}

scanForViruses()
{
    echo -e "${color}::${reset} Scanning for viruses..."

    # Update definitions
    sudo freshclam || ( echo "Count not update virus definitions!"; exit )

    # Run scan
    clamscan -r / || ( echo "Could not perform a virus scan!"; exit)
}

promptForVirusScan()
{
    read -r -p "${color}::${reset} Scan for viruses? [y/N] " response

    case "$response" in
        [yY][eE][sS]|[yY] ) scanForViruses;;
                        * ) ;;
    esac
}

finalize()
{
    echo -e "Done! Finalizing..."

    cd ${HOME}

    exit
}

emptyTrash;
runSystemUpgrade;
cleanSystemCache;
cleanPackageCache;
cleanLint;
cleanJournal;
runBleachbit;
cleanupOrphanedPackages;
fixPackageRepository;
promptForDefrag;
promptForVirusScan;

finalize
